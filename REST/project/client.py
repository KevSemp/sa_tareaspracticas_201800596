
# request library 

import requests
import os
from dotenv import load_dotenv
load_dotenv()

# environment variables that save the url to make the request and the authtoken
REST_URL = os.getenv('REST_URL')
AUTH_TOKEN = os.getenv('AUTH_TOKEN')

# method to get user by id, it respond with code 200 if it's correct, to verify if it exist
def getUser(id):
    hed = {'Accept': 'application/json','Content-Type':'application/json','Authorization': 'Bearer ' + AUTH_TOKEN}
    url = REST_URL+"/"+str(id)

    try:
        response = requests.get(url, headers=hed)
        if response.status_code != 200:
            print("el usuario no existe")
            print(response.json()['data']['message'])
            return
        print("el usuario existe, es:  ")
        print(response.json()['data'])
    except:
        print("Algo salió mal")



# method to set users, it responds with the code 201 if it is correct
def insertUser(name,gender,email,status):
    hed = {'Accept': 'application/json','Content-Type':'application/json','Authorization': 'Bearer ' + AUTH_TOKEN}
    data = {'name' : name,'gender': gender,"email":email,"status":status}
    url = REST_URL
    try:
        response = requests.post(url, json=data, headers=hed)
        if response.status_code != 201:
            print("no ha sido posible ingresar el usuario")
            print(response.json()['data']['message'])
            return
        print("datos del usuario ingresado: ")
        dataUser = response.json()['data']
        print(dataUser)
        getUser(dataUser['id'])   #to verify if it exist
    except:
        print("Algo salió mal")

# method to update user by id
def updateUser(id,name,gender,email,status):
    hed = {'Accept': 'application/json','Content-Type':'application/json','Authorization': 'Bearer ' + AUTH_TOKEN}
    data = {'name' : name,'gender': gender,"email":email,"status":status}
    url = REST_URL+"/"+str(id)
    try:
        response = requests.patch(url, json=data, headers=hed)
        if response.status_code != 200:
            print("error al actualizar el usuario")
            print(response.json()['data']['message'])
            return
        dataUser = response.json()['data']
        print(dataUser)
        getUser(dataUser['id'])   #to verify if it exist   
    except:
        print("Algo salió mal")


# method to delete user by id
def deleteUser(id):
    hed = {'Accept': 'application/json','Content-Type':'application/json','Authorization': 'Bearer ' + AUTH_TOKEN}
    url = REST_URL+"/"+str(id)
    try:
        response = requests.delete(url, headers=hed)
        if response.status_code != 204:
            print("error al eliminar el usuario")
            return
        print("el usuario ha sido eliminado correctamente")
        getUser(id) #to verify if not exist
    except:
        print("Algo salió mal")
        

def init():
 #Console menu
    isFinish = False
    while not isFinish:
        print("======================")
        print("1. Ingresar Usuario")
        print("2. Update user")
        print("3. Delete user")
        print("4. Salir")
        option = input("Escribe la opción que deseas: ")
        print("======================")
        if option == "1":  #insert user option
            print("Ingresar usuario")
            name = input("Escribe tu nombre: ")
            email = input("Escribe tu email: ")
            print("gender")
            print("1. Male")
            print("2. Female")
            option_gender = input("Escribe la opción deseada: ")
            gender = "female"
            if option_gender == "1":
                gender = "male"
            insertUser(name,gender,email,"active")

        elif option == "2":  #option, update user
            print("Actualizar usuario")
            id_user = input("Ingrese ID de usuario: ")
            name = input("Ingrese nuevo nombre de usuaro: ")
            email = input("Ingrese nuevo email de usuario: ")
            print("gender")
            print("1. Male")
            print("2. Female")
            option_gender = input("Escribe la opción deseada: ")
            gender = "female"
            if option_gender == "1": #in case if user select male
                gender = "male"
            updateUser(id_user,name,gender,email,"active")      

        elif option == "3": #option , delete user
            print("Eliminar usuario")
            id_user = input("Ingrese id de usuario a eliminar: ")
            deleteUser(id_user)

        elif option == "4":
            print("Hasta Luego")
            isFinish = True
       
        else : 
            print("selecciona una opción valida")



if __name__ == "__main__":
    init()


