
import zeep

#object client zeep to connect with http://www.dneonline.com/calculator.asmx
soap = zeep.Client(wsdl="http://www.dneonline.com/calculator.asmx?WSDL", service_name="Calculator",port_name="CalculatorSoap12")



#Aritmetic methods, only needs 2 numbers, all of them connect to the respective service, and print the result
def add(a,b):
    try:
        result = soap.service.Add(a, b)
        print("resultado de "+a+" + "+b+" es:")
        print(result)
    except:
        print("Algo salió mal")

def divide(a,b):
    try:
        if b==0:
            print("error no es posible dividir entre 0")
            return
        result = soap.service.Divide(a, b)
        print("resultado de "+a+" / "+b+" es:")
        print(result)
    except:
        print("Algo salió mal")

def subtract(a,b):
    try:
        print("resultado de "+a+" - "+b+" es:")
        result = soap.service.Subtract(a, b)
        print(result)
    except:
        print("Algo salió mal")

def multiply(a,b):
    try:
        result = soap.service.Multiply(a, b)
        print("resultado de "+a+" * "+b+" es:")
        print(result)
    except:
        print("Algo salió mal")



def init():
 #Console menu
    isFinish = False
    while not isFinish:
        print("======================")
        print("1. Sumar")
        print("2. Restar")
        print("3. Multiplicar")
        print("4. Dividir")
        print("5. Salir")
        option = input("Escribe la opción que deseas: ")
        print("======================")
        if option != "5":
            firstNumber = input("Ingresa el primer número: ")
            secondNumber = input("Ingresa el segundo número: ")
        if option == "1":  #option, add
            add(firstNumber,secondNumber)
        elif option == "2":  #option, subtract
            subtract(firstNumber,secondNumber)  
        elif option == "3": #option , multiply
            multiply(firstNumber,secondNumber)
        elif option == "4": #option , divide
            divide(firstNumber,secondNumber)
        elif option == "5":
            isFinish = True
        else : 
            print("selecciona una opción valida")



if __name__ == "__main__":
    init()