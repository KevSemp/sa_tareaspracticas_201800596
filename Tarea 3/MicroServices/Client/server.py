# request library 


import requests



from flask import Flask, jsonify, request
from flask_cors import CORS  # pip install -U flask-cors
app = Flask(__name__)
CORS(app)


#method to post order to restaurant
@app.route('/order',methods=['POST'])
def set_order():
    print(request.json)
    url = "http://localhost:5002/order"   
    response = requests.post(url, json=request.json)
    return {'msg': response.json()["msg"]}


#path to get status order from restaurant
@app.route('/order_restaurant/<order_id>',methods=['GET'])
def get_order_restaurant(order_id):
    url = "http://localhost:5002/order"+"/"+order_id
    response = requests.get(url)
    data = {"estado":response.json()["estado"]}
    return data

#path to get status order from dalivery
@app.route('/order_delivery/<order_id>',methods=['GET'])
def get_orde_delivery(order_id):
    url = "http://localhost:5001/order"+"/"+order_id
    response = requests.get(url)
    data = {"estado":response.json()["estado"]}
    return data



if __name__ == '__main__':
    app.run()
