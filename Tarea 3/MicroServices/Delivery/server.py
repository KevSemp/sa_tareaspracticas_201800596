# request library 


import requests



from flask import Flask, jsonify, request
from flask_cors import CORS  # pip install -U flask-cors
app = Flask(__name__)
CORS(app)
orders = []

#path to save order
@app.route('/order',methods=['POST'])
def set_order():
    print(request.json)
    global orders
    orders.append({"order":request.json[0]["order"],"estado":"en entrega"})
    print("orden guardada en rapartidor")
    return 'Orden recibida'

#path to get the status
@app.route('/order/<order_id>',methods=['GET'])
def get_status(order_id):
    global orders
    print(order_id)
    print(orders)
    order = [p for p in orders if p["order"]["id"] == order_id]
    print("estado de orden con repartidor")
    print(order[0]["estado"])
    data = {
        "msg": "orden de repartidor",
        "estado":order[0]["estado"]
    }
    return data


#path to change the order to entragada
@app.route('/order_delivered/<order_id>',methods=['PUT'])
def change_status(order_id):
    global orders
    print("cambio de estado de orden a entregado")
    for order in orders:
        if(order['order']['id'] == order_id):
            order['estado'] = 'entregado'
    return "orden entregada"



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
