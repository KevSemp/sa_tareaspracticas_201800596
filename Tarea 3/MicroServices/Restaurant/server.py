# request library 


import requests



from flask import Flask, jsonify, request
from flask_cors import CORS  # pip install -U flask-cors
app = Flask(__name__)
CORS(app)

orders = []

#path to save order
@app.route('/order',methods=['POST'])
def set_order():
    global orders
    orders.append({"order":request.json,"estado":"preparacion"})
    print("order saved")
    print(orders)
    print("==========")
    return {"msg":"orden guardada"}


#path to get the status of the order
@app.route('/order/<order_id>',methods=['GET'])
def get_status(order_id):
    global orders
    order = [p for p in orders if p["order"]["id"] == order_id]
    print("order from restaurant")
    print(order)
    data = {
        "msg": "orden en restaurante",
        "estado":order[0]["estado"]
    }
    return data

#method to send the order to delivery
@app.route('/send_order',methods=['POST'])
def send_order():
    global orders
    order_id = request.json["id"]
    order = [p for p in orders if p["order"]["id"] == order_id]
    print(order)
    url = "http://localhost:5001/order"
    requests.post(url, json=order)
    for order in orders:
        if(order['order']['id'] == order_id):
            order['estado'] = 'enviado a repartidor'
    return {'msg':'order sended'}


@app.route('/')
def hello_world():
    return 'Hello, World!'



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5002)
