terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  credentials = file("labsa-343000-581c151cdd8f.json")

  project = "labsa-343000"
  region  = "us-central1"
  zone    = "us-central1-a"
}

resource "google_compute_firewall" "rules" {
  name        = "my-firewall-rule"
  network     = "default"
  
  description = "Creates firewall rule targeting tagged instances"

  allow {
    protocol  = "tcp"
    ports     = ["80", "8080", "3000"]
  }

  source_ranges = ["0.0.0.0/0"] # Not So Secure. Limit the Source Range
  source_tags = ["infire"]
  target_tags = ["outfire"]
}

resource "google_compute_instance" "kevs" {
  name         = "kevserver"
  machine_type = "e2-medium"
  tags         = ["outfire","infire"]
  boot_disk {
    initialize_params {
      image="ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

  metadata_startup_script = "sudo mkdir hola"
  network_interface {
    network = "default"
    access_config {
     //prueba
    }
  }


   metadata ={     
      ssh-keys ="kevincardona6812:${file("C:\\Users\\kevin\\.ssh\\id_rsa.pub")}"   
      }

}