# request library 



import requests
import json
from flask import Flask, jsonify, request
from flask_cors import CORS  # pip install -U flask-cors
app = Flask(__name__)
CORS(app)
import mysql.connector

#Cónexión con la base de datos
mydb = mysql.connector.connect(
        host = "35.223.5.223",
        user = "root",
        password = "root",
        database = "tareaSA"
)

mycursor = mydb.cursor()


#Método que permite insertar un usuario 
@app.route('/user',methods=['POST'])
def set_flight():
    print(request.json)
    sql = "INSERT INTO Usuario (nombre, carnet) VALUES (%s, %s)"
    val = (request.json['nombre'],request.json['carnet'])
    mycursor.execute(sql, val)
    mydb.commit()
    print(mycursor.rowcount, "record inserted.")
    return {"data":"data"}

#Método que permite obtener todos los estudiantes
@app.route('/user',methods=['GET'])
def get_flights():
    sql = "SELECT * FROM Usuario"
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    users = []
    for x in myresult:
         users.append({
         "id":x[0],
        "nombre": x[1],
        "carnet": x[2]
         })
    print(users)
    return {"data":users}    

#Método que permite actualizar los datos de un estudiante
@app.route('/user/<user_id>',methods=['PUT'])
def update_user(user_id):
    print(user_id)
    print(request.json)

    sql ="UPDATE Usuario SET nombre = '"+request.json["nombre"] + "', carnet ="+request.json["carnet"] +" WHERE usuario_id = " + user_id
    print(sql)
    mycursor.execute(sql)
    mydb.commit()
    return "hello"

#Método que permite eliminar un estudiante
@app.route('/user/<user_id>',methods=['DELETE'])
def delete_user(user_id):
    print(user_id)
    sql = "DELETE FROM Usuario WHERE usuario_id ="+ user_id
    mycursor.execute(sql)
    mydb.commit()
    return "hello"








if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port="5000")
