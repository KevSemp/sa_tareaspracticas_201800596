# Tarea Práctica 9

## Pruebas de funcionalidad


### Docker hub images
- https://hub.docker.com/repository/docker/kevsemp/tarea9_back



- https://hub.docker.com/repository/docker/kevsemp/tarea9_front



## Flujo

### video demostración de flujo
https://drive.google.com/file/d/16n4y0kPKNMyEpNLANTSfQRaOlsecomCv/view?usp=sharing







- Pipeline develop , donde se puede observar que el job es ejecutado exitosamente

![title](images/img-6.PNG)




- Pipeline main, donde se puede observar que el job es ejecutado exitosamente

![title](images/img-7.PNG)

-  Log del stage build, que se encarga de construir las imagenes y correr contenedores dentro de la máquina virtual de desarrollo

![title](images/img-8.PNG)


- Log del stage publish, que permtie publicar las imagenes a dockerhub


![title](images/img-9.PNG)

- Log del stage deploy, que permite realizar un pull de las imagenes de docker hub dentro de la máquina de producción y levantar dichos contenedores


![title](images/img-10.PNG)


- Log del stage package, donde se generan los artefactos


![title](images/img-11.PNG)


- Artefactos generados

![title](images/img-12.PNG)


- Deploy de aplicación de react

![title](images/img-13.PNG)









### Script

- .gitlab-ci.yml

- Stages, aqui se define cada uno de los stages.
  **build**: donde todas las imagenes seran construidas y cada contenedor sera ejecutado por medio de un docker compose
  **pubish:** donde todas las imagenes creadas son enviadas a docker hub
  **deploy**: dondo las imagenes son descargadas de docker hub dentro de la máquina de producción
  **packege**: stage que permite generar los artefactos 

```
stages:
    - build
    - publish
    - deploy
    - package

```

- Stage build
```
build:
    stage: build
    only:
        - "develop"
    environment:
        name: develop 
    tags:
        - saprac 
    before_script:
        - cd tarea7
        - docker-compose down --rmi all  
        - cd ..            
    script:
        - cd tarea7
        - docker-compose up --build -d

```

- Stage publish
```
publish:
    stage: publish
    only:
        - "develop"
    environment:
        name: develop 
    tags:
        - saprac      
    script:
        - docker login -u kevsemp -p ae218ff7-3b5c-4697-bed0-6d75d330ee3b
        - docker push kevsemp/tarea9_front:latest
        - docker push kevsemp/tarea9_back:latest

```
- Deploy production

```
deploy-production:
    stage: deploy
    only:
        - "main" 
    tags:
        - practica     
    script:
        - docker login -u kevsemp -p ae218ff7-3b5c-4697-bed0-6d75d330ee3b
        - docker pull "kevsemp/tarea9_front:latest" 
        - docker run -d -p 0.0.0.0:8080:80 --name=front kevsemp/tarea9_front:latest
        - docker pull "kevsemp/tarea9_back:latest"
        - docker run -d -p 0.0.0.0:3000:5000 --name=back kevsemp/tarea9_back:latest
    after_script:
        - docker logout
```

- Stage packege

```
package:
    stage: package
    only:
        - "main"    
    tags:
        - saprac
    script:
        - echo "artifacts"         
    artifacts:
        paths:
            - tarea7/Backend
            - tarea7/my-app
        exclude:
            - tarea7/my-app/src/App.test.js


```



