import React, { Component } from "react";
import "./student_form.css";

import { Form, Button, Card, Col, Row, Table } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from 'axios';
import {BsFillTrashFill} from "react-icons/bs";
import {AiFillEdit} from "react-icons/ai";

class Form_Assist extends Component {
  constructor() {
    super();
    this.state = {
      Carnet: "",
      Nombre: "",
      carnet: "",
      data: [
        { id:1,carnet: "201800596", nombre: "Kevin" },
        { id:2,carnet: "201800123", nombre: "Pedro" },
      ],
    };
  }

  componentDidMount() {
      this.getData();
  }


  getData() {
    axios.get(`http://35.223.5.223:3000/user`)
    .then(res => {
      console.log(res);
      let data  = [];
      data = res.data.data;
      console.log(data)
      this.setState({data:data});
    })
  }

  handleSearchCarnet() {
    let { carnet } = this.state;
    const resultado = []
    resultado.push(this.state.data.find( student => student.Carnet === carnet ))
    this.setState({data:resultado});
  }



  handleNombreChange = (e) => {
    this.setState({ Nombre: e.target.value });
  };

  handleCarnetSChange = (e) => {
    this.setState({ carnet: e.target.value });
  };

  handleCarnetChange = (e) => {
    this.setState({ Carnet: e.target.value });
  };

    handleCarnetChange = (e) => {
    this.setState({ Carnet: e.target.value });
  };

  handleSubmit() {
    let { Nombre, Carnet } = this.state;

    let student_assist = {
      "carnet":Carnet,
      "nombre":Nombre
    };

    axios.post('http://35.223.5.223:3000/user',student_assist).then(res => {
      console.log(res);
    })

    this.state.data.push(student_assist);
    this.setState({data:this.state.data});
  }

  deleteStudent(id) {
    axios.delete(`http://35.223.5.223:3000/user/${id}`)
    .then(res => {
          console.log(res);
          this.getData();
    })
  }

  //tabe list student

  render() {
    const listItems = this.state.data.map((list_student) => (
      <tr key={list_student.id}>
        <td>{list_student.carnet}</td>
        <td>{list_student.nombre}</td>
        <td><Button variant="danger" onClick={() => this.deleteStudent(list_student.id)}><BsFillTrashFill/></Button> <Button variant="warning"><AiFillEdit/></Button></td>
      </tr>
    ));

    return (
      <>
        <br></br>
        <div className="App-center">
          <Row className='mb-5'>
          <Card
            text="light"
            bg="dark"
            style={{ width: "50rem", justify: "center" }}
          >
            <Card.Body>
              <Card.Title>201800596 Tarea Práctica 7 </Card.Title>

              <Form>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Row>
                    <Col>
                      <Form.Label>Carnet</Form.Label>
                      <Form.Control
                        name="carnet"
                        type="text"
                        placeholder=""
                        value={this.state.Carnet}
                        onChange={this.handleCarnetChange}
                      />
                    </Col>
                    <Col>
                      <Form.Label>Nombre</Form.Label>
                      <Form.Control
                        name="nombre"
                        type="text"
                        value={this.state.Nombre}
                        onChange={this.handleNombreChange}
                        placeholder=""
                      />
                    </Col>
                  </Row>
                </Form.Group>

                <Button name="button_submit" onClick={() => this.handleSubmit()}>Ingresar</Button>
              </Form>
            </Card.Body>
          </Card>
          </Row>
        </div>
        <div>
          
          <Row className="justify-content-md-center">
            <Col sm={12}>
              <div>
                <Table responsive striped bordered hover variant="dark">
                  <thead>
                    <tr>
                      <th scope="col ">Carnet</th>
                      <th scope="col">Nombre</th>
                    </tr>
                  </thead>
                  <tbody>{listItems}</tbody>
                </Table>
              </div>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Form_Assist;
